const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { addError, addErrorNotFound } = require('../services/common');

const router = Router();

router.post('/', createUserValid, (req, res, next) => {
    try {
        res.locals.resObject = UserService.create(res.locals.user);
        next();
    } catch (err) {
        addError(res, err.message);
        return;
    }
}, responseMiddleware);

router.get('/', (req, res, next) => {
    res.locals.resObjects = UserService.getAll();
    next();
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        const user = UserService.searchById(req.params.id);
        if (user) {
            res.locals.resObject = user;
            next();
        } else {
            addErrorNotFound(res, `User id '${req.params.id}' not found`);
        }
    } catch (err) {
        addError(res, err.message);
        return;
    }
}, responseMiddleware);

router.put('/:id', updateUserValid,  (req, res, next) => {
    try {
        const user = UserService.updateUser(req.params.id, res.locals.user);
        if (user) {
            res.locals.resObject = user;
            next();
        } else {
            addErrorNotFound(res, `User id '${req.params.id}' not found`);
        }
    } catch (err) {
        addError(res, err.message);
        return;
    }
}, responseMiddleware);

router.delete('/:id', (req, res) => {
    try {
        const deletedUsers = UserService.deleteUser(req.params.id);
        if (deletedUsers.length > 0) {
            res.send(null);
            return;
        } else {
            addErrorNotFound(res, `User id '${req.params.id}' not found`);
        }
    } catch (err) {
        addError(res, err.message);
        return;
    }
});

module.exports = router;