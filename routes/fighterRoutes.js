const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');
const { addError, addErrorNotFound } = require('../services/common');

const router = Router();

// TODO: Implement route controllers for fighter
router.post('/', createFighterValid, (req, res, next) => {
    try {
        res.locals.resObject = FighterService.create(res.locals.fighter);
        next();
    } catch (err) {
        addError(res, err.message);
        return;
    }
}, responseMiddleware);

router.get('/', (req, res, next) => {
    res.locals.resObjects = FighterService.getAll();
    next();
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        const fighter = FighterService.search({ id: req.params.id });
        if (fighter) {
            res.locals.resObject = fighter;
            next();
        } else {
            addErrorNotFound(res, `Fighter id '${req.params.id}' not found`);
        }
    } catch (err) {
        addError(res, err.message);
        return;
    }
}, responseMiddleware);

router.put('/:id', updateFighterValid,  (req, res, next) => {
    try {
        const fighter = FighterService.updateFighter(req.params.id, res.locals.fighter);
        if (fighter) {
            res.locals.resObject = fighter;
            next();
        } else {
            addErrorNotFound(res, `Fighter id '${req.params.id}' not found`);
        }
    } catch (err) {
        addError(res, err.message);
        return;
    }
}, responseMiddleware);

router.delete('/:id', (req, res) => {
    try {
        const deletedFighters = FighterService.deleteFighter(req.params.id);
        if (deletedFighters.length > 0) {
            res.send(null);
            return;
        } else {
            addErrorNotFound(res, `Fighter id '${req.params.id}' not found`);
        }
    } catch (err) {
        addError(res, err.message);
        return;
    }
});

module.exports = router;