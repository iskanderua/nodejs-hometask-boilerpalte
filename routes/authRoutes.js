const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { addError } = require('../services/common')

const router = Router();

router.post('/login', (req, res, next) => {
    try {
        // TODO: Implement login action (get the user if it exist with entered credentials)
        res.locals.resObject = AuthService.login(req.body);
        next();
    } catch (err) {
        addError(res, err.message);
        return;
    }
}, responseMiddleware);

module.exports = router;