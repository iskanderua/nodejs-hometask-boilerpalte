const { FighterRepository } = require('../repositories/fighterRepository');
const { cloneDeep } = require('./common');

class FighterService {
    create(fighter) {
        this.checkFighter(fighter);
        const new_fighter = FighterRepository.create(fighter);
        return cloneDeep(new_fighter);
    }

    nameDuplication({ name }) {
        return !! this.search( { name });
    }
    searchById(id) {
        return this.search({ id: id });
    }
    search(search) {
        const item = FighterRepository.getOne(search);
        if (!item) {
            return null;
        }
        return cloneDeep(item);
    }

    getAll() {
        return cloneDeep(FighterRepository.getAll());
    }
    
    updateFighter(id, fighter) {
        this.checkFighter(fighter);
        if (!this.searchById(id)) {
            return null;
        }
        const newFighter = FighterRepository.update(id, fighter);
        return cloneDeep(newFighter);
    }

    checkFighter(fighter) {
        if (this.nameDuplication(fighter)) {
            throw Error(`Fighter ${fighter.name} already exists`);
        }
    }
    
    deleteFighter(id) {
        const deletedFighters = FighterRepository.delete(id);
        return deletedFighters;
    }
}

module.exports = new FighterService();