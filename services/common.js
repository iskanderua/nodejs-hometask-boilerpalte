const deleteId = function (obj) {
    delete obj.id;
    return obj;
}
const toModel = function (model, obj, allProperties = true) {
    res = {};
    for (field in obj) {
        if (!model.hasOwnProperty(field)) {
            throw Error(`Unknown field '${field}'`);
        }
    }
    for (field in model) {
        if (allProperties || obj[field]) {
            res[field] = obj[field];
        }
    }
    return res;
}
const checkMandatoryFields = function (model, obj, skip) {
    for (field in model) {
        if (!obj[field] && !skip.includes(field)) {
            throw Error(`${field} is mandatory`);
        }
    }
}

const cloneDeep = obj => JSON.parse(JSON.stringify(obj));
const addError = (res, message) => res.status(400).send({ error: true, message: message });
const addErrorNotFound = (res, message) => res.status(404).send({ error: true, message: message });

exports.addError = addError;
exports.addErrorNotFound = addErrorNotFound;
exports.cloneDeep = cloneDeep;
exports.checkMandatoryFields = checkMandatoryFields;
exports.toModel = toModel;
exports.deleteId = deleteId;