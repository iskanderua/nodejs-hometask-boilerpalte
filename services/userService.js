const { UserRepository } = require('../repositories/userRepository');
const { cloneDeep } = require('./common');

class UserService {
    create(user) {
        this.checkUser(user);
        const newUser = UserRepository.create(user);
        return cloneDeep(newUser);
    }

    emailDuplication({ email }) {
        const user = this.search({ email: email });
        return !!user;
    }

    phoneNumberDuplication({ phoneNumber }) {
        const user = this.search({ phoneNumber: phoneNumber });
        return !!user;
    }
    searchById(id) {
        return this.search({ id: id });
    }
    search(search) {
        const item = UserRepository.getOne(search);
        if (!item) {
            return null;
        }
        return cloneDeep(item);
    }

    getAll() {
        return cloneDeep(UserRepository.getAll());
    }

    updateUser(id, user) {
        this.checkUser(user);
        if (!this.searchById(id)) {
            return null;
        }
        const newUser = UserRepository.update(id, user);
        return cloneDeep(newUser);
    }

    checkUser(user) {
        if (user.email && this.emailDuplication(user)) {
            throw Error(`User ${user.email} already exists`);
        }
        if (user.phoneNumber && this.phoneNumberDuplication(user)) {
            throw Error(`User with phone number ${user.phoneNumber} already exists`);
        }
    }
    deleteUser(id) {
        const deletedUsers = UserRepository.delete(id);
        return deletedUsers;
    }
}

module.exports = new UserService();