const { deleteId } = require('../services/common');

const responseMiddleware = (req, res, next) => {
    if (res.locals.resObject) {
        res.json(/*deleteId*/(res.locals.resObject));
    } else {
        res.json(res.locals.resObjects /*.map(deleteId)*/);
    }
    next();
}

exports.responseMiddleware = responseMiddleware;