const { fighter: fighterModel } = require('../models/fighter');
const { addError, toModel, checkMandatoryFields } = require('../services/common')

const createFighterValid = (req, res, next) => {
    if (!req || !req.body) {
        addError(res, 'No request found');
        return;
    }
    try {
        const fighter = toModel(fighterModel, req.body);
        checkMandatoryFields(fighterModel, fighter, skip = ['health', 'id']);
        if (!fighter.health) {
            fighter.health = 100;
        }
        checkFighter(fighter);
        res.locals.fighter = fighter;
        next();
    } catch (err) {
        addError(res, err.message);
        return;
    }
}
const checkFighter = (fighter) => {
    if (fighter.id) {
        throw Error('Id should not be set');
    }
    if (fighter.power) {
        fighter.power = +fighter.power;
        if (!fighter.power || fighter.power < 1 || fighter.power > 100) {
            throw Error('Power should be a number in a range 1..100');
        }
    }
    if (fighter.defense) {
    fighter.defense = +fighter.defense;
    if (!fighter.defense || fighter.defense < 1 || fighter.defense > 10) {
        throw Error('Defense should be a number in a range 1..10');
    }
    }
    if (fighter.health) {
        fighter.health = +fighter.health;
        if (fighter.health < 80 || fighter.health > 120) {
            throw Error('Health should be a number in a range 80..120');
        }
    }
}
const updateFighterValid = (req, res, next) => {
    if (!req || !req.body) {
        addError(res, 'No request found');
        return;
    }
    try {
        const fighter = toModel(fighterModel, req.body, allProperties = false);
        if (Object.keys(fighter).length == 0) {
            addError(res, 'No data to update');
            return;
        }
        checkFighter(fighter);
        res.locals.fighter = fighter;
        next();
    } catch (err) {
        addError(res, err.message);
        return;
    }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;