const { user: userModel } = require('../models/user');
const { addError, toModel, checkMandatoryFields } = require('../services/common');

const createUserValid = (req, res, next) => {
    if (!req || !req.body) {
        addError(res, 'No request found');
        return;
    }
    try {
        const user = toModel(userModel, req.body);
        checkMandatoryFields(userModel, user, ['id']);
        checkUser(user);
        res.locals.user = user;
        next();
    } catch (err) {
        addError(res, err.message);
        return;
    }
}
const checkUser = (user) => {
    if (user.id) {
        throw Error('Id should not be set');
    }
    if (user.email && !user.email.endsWith('@gmail.com')) {
        throw Error('Only gmail accepted');
    }
    if (user.phoneNumber && !user.phoneNumber.startsWith('+380') || user.phoneNumber.length != 13) {
        throw Error('Phone number format: +380xxxxxxxxx');
    }
    if (user.password && user.password.length < 3) {
        throw Error('Min password length is 3');
    }
}
const updateUserValid = (req, res, next) => {
    if (!req || !req.body) {
        addError(res, 'No request found');
        return;
    }
    try {
        const user = toModel(userModel, req.body, allProperties = false);
        if (Object.keys(user).length == 0) {
            addError(res, 'No data to update');
            return;
        }
        checkUser(user);
        res.locals.user = user;
        next();
    } catch (err) {
        addError(res, err.message);
        return;
    }
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;